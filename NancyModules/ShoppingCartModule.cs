﻿using Nancy;
using Nancy.ModelBinding;
using ShoppingCartMicroservice.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCartMicroservice.NancyModules
{
    public class ShoppingCartModule : NancyModule
    {
        public ShoppingCartModule(IShoppingCartStore shoppingCartStore, 
            IProductCatalogClient productCatalog,
            IEventStore eventStore)
            : base("/shoppingcart")
        {
            Get("/{userId:int}", parameters => 
            {
                var userId = (int)parameters.userid;
                return shoppingCartStore.Get(userId);
            });

            Post("/{usreId:int}/items", async (parameters, _) =>

            {
                var productCatalogIds = this.Bind<int[]>();
                var userId = (int)parameters.userid;

                var shoppingCart = shoppingCartStore.Get(userId);
                var shoppingCartItems = await
                    productCatalog
                        .GetShoppingCartItems(productCatalogIds)
                        .ConfiguserAwait(false);
                shoppingCart.AddItems(shoppingCartItems, eventStore);

                return shoppingCart;
            });
        }
    }
}
