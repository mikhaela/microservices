﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCartMicroservice.Models;

namespace ShoppingCartMicroservice.Services
{
    public interface IShoppingCartStore
    {
        ShoppingCart Get(int userId);
    }

    public class ShoppingCartStore : IShoppingCartStore
    {
        public ShoppingCart Get(int userId)
        {
            return new ShoppingCart() { UserId = userId };
        }
    }
}
