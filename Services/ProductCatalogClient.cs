﻿using ShoppingCartMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCartMicroservice.Services
{
    public interface IProductCatalogClient
    {
        async IEnumerable<ShoppingCartItem> GetShoppingCartItems(IEnumerable<int> productCatalogIds);
    }

    public class ProductCatalogClient : IProductCatalogClient
    {
        public async IEnumerable<ShoppingCartItem> GetShoppingCartItems(IEnumerable<int> productCatalogIds)
        {
            return new List<ShoppingCartItem>();
        }
    }
}
